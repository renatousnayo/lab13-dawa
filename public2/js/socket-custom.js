var socket = io();

socket.on("connect", function () {
    console.log("Conectado al servidor");
});

socket.on("disconnect", function () {
    console.log("Perdimos conexion con el servidor");
});

socket.emit(
    "enviarMensaje",
    {
        usuario: "Renato",
        mensaje: "Hola Mundo",
    }
);

socket.on("enviarMensaje", function (mensaje) {
    console.log("Servidor mensaje:", mensaje);
});